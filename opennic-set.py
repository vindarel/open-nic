#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import sys
import shutil
import platform
import argparse
import subprocess
import urllib2
import json
import datetime
import socket
import string
import time

"""
Use dns servers of the open-nic project.
Get the nearest dns servers and insert them into your configuration.

usage (Debian): sudo python opennic-set.py
usage (Mageia): $ su
              : # python opennic-set.py
Requirements (on Debian):
apt-get install resolvconf # installed in Ubuntu
Requirements (on Mageia):
# urpmi resolvconf
Full information: https://github.com/vindarel/open-nic 
This version is heavly modified by kensan: kensan@kensan.it

Mageia Installation:
put in /etc/rc.d/rc.local the follow line:
/usr/bin/python /home/sandro/bin/opennic-set.py > /home/sandro/bin/log/opennic.log&
where "sandro" is your username and bin is your script folder (bin/log is your log folder)
execute:
# python opennic-set.py
verify that is all fine and then reboot and shutdown the linux-box.
(router may be on, so shutdown for test)
At restart check Internet and ~/bin/log/opennic.log.
"""

DIR_RESOLVCONF = "/etc/resolvconf/resolv.conf.d/"
DEBIAN_MAGEIA_CONF = DIR_RESOLVCONF + "head"
RESOLVCONF = "/sbin/resolvconf"

#Default DNS server
DNS_SET_NUM = 3 # Number of set of Default DNS
DEFAULT_DNS = [i for i in range(DNS_SET_NUM)]
DEFAULT_DATA_DNS = [i for i in range(DNS_SET_NUM)]
DEFAULT_DNS_NUM = [i for i in range(DNS_SET_NUM)]

#SET 1 of DNS
DEFAULT_DNS[0] = ["193.70.192.25", "193.70.192.15",] #DNS Infostrada without 404 redirect (DNS hijacking)
DEFAULT_DATA_DNS[0] = ["DNS1 Wind without 404 redirect (DNS hijacking)", "DNS2 Wind without 404 redirect (DNS hijacking)"]
DEFAULT_DNS_NUM[0] = 2

#SET 2 of DNS
DEFAULT_DNS[1] = ["8.8.8.8", "8.8.4.4",] #DNS google
DEFAULT_DATA_DNS[1] = ["DNS1 google", "DNS2 google"]
DEFAULT_DNS_NUM[1] = 2

#SET 3 of DNS
DEFAULT_DNS[2] = ["208.67.222.222", "208.67.220.220", "208.67.222.220", "208.67.220.222"] #DNS openDNS
DEFAULT_DATA_DNS[2] = ["resolver1.opendns.com", "resolver2.opendns.com", "resolver3.opendns.com", "resolver4.opendns.com"]
DEFAULT_DNS_NUM[2] = 4

#Datas for testing Internet vai IP or via domain name
TEST_NUM = 4
PING_IP = [i for i in range(TEST_NUM)]
TEST_INTERNET_DOMAIN = [i for i in range(TEST_NUM)]
# domain always up
TEST_INTERNET_DOMAIN[0] = "startpage.com"
TEST_INTERNET_DOMAIN[1] = "google.com"
TEST_INTERNET_DOMAIN[2] = "opendns.com"
TEST_INTERNET_DOMAIN[3] = "kensan.it"
# IP always up
PING_IP[0] = "193.70.192.25" #Wind Italy DNS
PING_IP[1] = "4.2.2.2" #It belongs to Level 3 and is anycast
PING_IP[2] = "208.67.222.222" #OpenDNS DNS
PING_IP[3] = "85.37.17.51" #Telecom Italy DNS

BACK_SUFFIX = ".opennicset-back"
NUMERO_DNS_OPENNIC = 4 # Number of openiNIC DNS to put in Resolv.conf text file and used for resolv domain name into ip
DNS_API_OPENNIC = "https://api.opennicproject.org/geoip/?jsonp&res="+str(NUMERO_DNS_OPENNIC)+"&ip=&nearest"

now = datetime.datetime.now()
LOCALTIME = now.strftime("%Y-%m-%d %H:%M:%S")

	
def getDnsList():
    """Gets your nearest openNIC servers from API
    (http://www.opennicproject.org/nearest-servers/).
    returns a list of IP addresses.
    """
    dns_list = [i for i in range(NUMERO_DNS_OPENNIC)]
    dns_data_list = [i for i in range(NUMERO_DNS_OPENNIC)]
    url = DNS_API_OPENNIC
    print "Connecting to %s..." % (url,)
    # get DNS opennic from Internet
    try :
      response = urllib2.urlopen(url)
      data = response.read()
      data = re.sub(r'([a-zA-Z_0-9\.]*\()|(\);?$)','',data) #transform jsonp into json
      data = json.loads(data)
    except :
      print "Error The OpenNIC DNS list is not accessible: www.opennicproject.org site down"
      return 0,0,0
    # load DNS opennic in array
    numero_dns = 0
    for i in range(NUMERO_DNS_OPENNIC):
      try:
	ip = data[i]['ip']
	valid_ip = is_valid_ip(ip)
      except:
	valid_ip = False
      if valid_ip:
	status = test(ip, "Warning: ip "+ip+" openNIC DNS down", 0)
	if status == "up":
	  dns_list[i] = ip
	  try:
	    loc = data[i]['loc']
	  except:
	    loc = "Unknown"
	  try:
	    stat = data[i]['stat']
	  except:
	    stat = "Unknown"
	  dns_data_list[i] = "OpenNIC - " + "loc: "+loc+", stat: "+stat+", "+LOCALTIME
	  numero_dns += 1
    return dns_list, dns_data_list, numero_dns


def is_valid_ip(ip):
    # check valid IPv4, to add valid IPv6
    m = re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
    return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))
  
  
def updateResolvconf():
    """resolvconf -u
    """
    print "Updating configuration…",
    subprocess.Popen(['resolvconf', '-u'])
    print " done."

def test_domain(message):
    for i in range(TEST_NUM):
      if test(TEST_INTERNET_DOMAIN[i], message, 1) == "up":
	return "up"
    return "down"


def test_ip(message):
    for i in range(TEST_NUM):
      if test(PING_IP[i], message, 1) == "up":
	return "up"
    return "down"


def test(domain_or_ip, error_message, step_number):
    """	Tests if we can access Internet
	domain_or_ip is google.com or 8.8.8.8
	step_number is the number of times that domain is pinging
	and it cost al least time_number seconds or more
    """
    i = 0
    while True:
      if ping(domain_or_ip) == "up":
	break
      i += 1
      if step_number != 0: time.sleep(1)
      if i >= step_number:
	      print error_message
	      return "down"
    return "up"


def ping(domain_or_ip):
    #ping one time domain or ip like google.com or 8.8.8.8
    #and then check the response...
    try:
      output = subprocess.check_output("ping -c 1 " + domain_or_ip, shell=True)
      print "Ping OK - " + domain_or_ip
      #hostname, 'is up!'
      return "up"
    except:
      #hostname, 'is down!'
      return "down"


def editConf(dns_list, dns_data_list, numero_dns, conf):
    """Writes the given dns servers to the given file.
    """
    back = conf + BACK_SUFFIX
    if os.path.exists(conf):
        if not os.path.exists(back):
            shutil.copyfile(conf, back)
    else:
        print "%s file not found. Did you install resolvconf ?" % (conf,)

    w = "".join("nameserver " + dns_list[i] + " # " + dns_data_list[i] + "\n" for i in range(numero_dns))
    
    with open(conf, 'r') as f:
        lines = f.readlines()
        headstring = ""
	for l in lines:
	  if not l.startswith("nameserver"):
	    headstring += l

    head = headstring + w

    with open(conf, 'w') as f:
        print "we  put the lines:\n" +  w + "\nto " + conf
        print "We saved the original configuration file, so if you notice any connection problem, you can still put it back with the --undo option."
        f.write(head)
        

def main(*args):
  
    CONF = DEBIAN_MAGEIA_CONF
    print "Local current time :", LOCALTIME
    print
    # waiting: Internet is up?
    # waiting timeout step_number in seconds
    step_number = 120
    for wait in range(step_number/TEST_NUM):
      status0 = test_ip("Warning: not connected to Internet")
      if status0 == "up":
	break
    if status0 == "down":
      print
      print "ERROR: Internet is down, please check your connection"
      print
      exit(1)
    print "Internet is UP!"
    # ok, Internet is up, is DNS server working?
    status1 = test_domain("Warning: Server DNS down")
    if status1 == "down":
      print "Server DNS is down, use default server DNS"
      #SET DNS number 1,2,3,..
      for i in range(DNS_SET_NUM):
	print
	print "Set Default DNS server number ", i+1
	editConf(DEFAULT_DNS[i], DEFAULT_DATA_DNS[i], DEFAULT_DNS_NUM[i], CONF)
	updateResolvconf()
	# is DNS up?
	status2 = test_domain("Warning: Server DNS down")
	if status2 == "down":
	  print "Warning: Default DNS server is down. Try the next..."
	else:
	  break
      # everyone default DNS server are down? or anyone is up?
      print
      if status2 == "up":
	print "Default DNS server setted and working!"
	print
      else:
	print "ERROR: NO WEB - DNS all DOWN: check your pc"
	print
	exit(1)
    else: #status1 == "up"
      print "DNS is UP!"
      print
      dns_list, dns_data_list, numero_dns = getDnsList()
      if numero_dns != 0:
	editConf(dns_list, dns_data_list, numero_dns, CONF)
	updateResolvconf()
	# ok, Internet is up, is OpenNIC DNS server working?
	status3 = test_domain("Warning: Server DNS OpenNIC down")
	if status3 == "up":
	  print
	  print "OpenNIC DNS server Setted and working!"
	  print
	else:
	  print
	  print "ERROR: NO WEB - OpenNIC DNS DOWN: check your pc"
	  print
	  exit(1)
      else:
	print
	print "WARNING: we couldn't get the nearest opennic servers from you."
	print "         Stay with old servers."
	print

def testOpennic():
    """Tests if we can access Internet via domain names
    """
    print "Testing… ",
    status = test_domain("Warning: Server DNS down")
    if status == "down":
      print "test FAILED: we can't access domain URL like google.com"
      return 1
    else: #status == "up"
      print "test SUCCESSFUL: we can access domain URL like google.com"
      return 0

def undo(conf_dir):
    """Put back the original configuration files."""
    files = ["tail", "head"]
    has_backup = False
    for f in files:
        f_path = conf_dir + f
        back = conf_dir + f + BACK_SUFFIX
        if os.path.exists(back):
            has_backup = True
            shutil.copyfile(back, f_path)
            print "backup of %s restored." % (f_path,)
            os.remove(back)

    if not has_backup:
        print "no backup found in %s" % (conf_dir,)

    return 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Use an Opennic DNS server. Usage: sudo python opennic-set.py")
    parser.add_argument("-t", "--test", action='store_true',
                    help="test if we can access an opennic's TLD")
    parser.add_argument("-u", "--undo", action='store_true',
                    help="put back the configuration files you had before starting playing with opennic-set.")

    args = parser.parse_args()
    if args.test:
        exit(testOpennic())

    if args.undo:
        exit(undo(DIR_RESOLVCONF))
    else:
        exit(main(sys.argv[1:]))
